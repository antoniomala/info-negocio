# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    
	url(r'^$', 'apps.website.views.home_page', name='home'),

	url(r'^cadastrar_usuario/$', 'apps.usuarios.views.cadastrar_usuario', name='cadastrar_usuario'),


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)