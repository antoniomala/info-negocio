# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import *
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse

def cadastrar_usuario(request):
	usuario = CadastrarUsuarioForm(request.POST or None)

	if usuario.is_valid():
		usuario.save()

		messages.success(request, u'Usuário cadastrado com sucesso')
		return HttpResponseRedirect(reverse('home'))
		
	return render(request, 'cadastrar_usuario.html', {'usuario': usuario})