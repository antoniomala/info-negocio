# -*- coding: utf-8 -*-
from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from localflavor.br.forms import STATE_CHOICES

class Usuario(User):
	nome = models.CharField(u'nome completo', max_length=120)
	estado = models.CharField(u'estado', max_length=55 ,choices=STATE_CHOICES)
	telefone = models.CharField(u'contato', max_length=50)
	status = models.BooleanField(default=True)

	# Fields de auditoria
	created_on = models.DateTimeField(auto_now_add=True)
	updated_on = models.DateTimeField(auto_now=True)


class CadastrarUsuarioForm(UserCreationForm):
	# sobrescrevendo algumas chamadas da classe pai
	def __init__(self, *args, **kwargs):
		super(CadastrarUsuarioForm, self).__init__(*args, **kwargs)
		# alterando o help_text do campo username
		self.fields['username'].help_text = ''
		self.fields['password2'].help_text = ''
	class Meta:

		model = Usuario
		fields = ('username', 'estado','nome', 'telefone')