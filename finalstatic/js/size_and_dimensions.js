// função de redimensionamento
function decidir_sumico(largura){
	// sumindo
	if(largura <= 625){
		$('#logman_logo').hide();
	}else{
		$('#logman_logo').show();
	}
}

$(document).ready(function(){
	// ocultando a logo da logman, caso o tamanho da tela seja inferior ao desejado
	largura = $(window).width();
	decidir_sumico(largura);
});

// no resize da página
$(window).resize(function(){
	// ocultando a logo da logman, caso o tamanho da tela seja inferior ao desejado
	largura = $(window).width();
	decidir_sumico(largura);
});